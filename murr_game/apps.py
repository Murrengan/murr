from django.apps import AppConfig


class MurrGameConfig(AppConfig):
    name = 'murr_game'

    def ready(self):
        import murr_game.signals
