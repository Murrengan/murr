import json
import os
import pathlib

murren_player_variables_path = os.path.join(pathlib.Path().absolute(),
                                            'murr_game/engine/murren_player_variables.json')

with open(murren_player_variables_path) as f:
    mpv = json.load(f)
