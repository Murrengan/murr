from enum import Enum

from django.contrib.auth import get_user_model
from django.db import models
from django_enum_choices.fields import EnumChoiceField

from murr_rating.models import RatingFields

Murren = get_user_model()


class SkillType(Enum):
    PHYSICAL = "physical"
    MAGICAL = "magical"
    INDEPENDENT = "independent"


class Skills(models.Model):
    name = models.CharField(max_length=256)
    icon = models.ImageField(blank=True, null=True, upload_to='murr_game/icon/')
    gif = models.ImageField(blank=True, null=True, upload_to='murr_game/gif/')
    description = models.TextField(max_length=1500)
    logic = models.TextField(max_length=500)
    cost = models.IntegerField()
    reload = models.IntegerField()
    base_damage = models.IntegerField()
    type = EnumChoiceField(SkillType)
    animation_cooldown = models.IntegerField()
    self_auto_use = models.BooleanField(default=False)
    sensitive_to_evasion = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Player(RatingFields):
    murren_player = models.ForeignKey(Murren, on_delete=models.CASCADE, related_name='murren_player')
    exp = models.IntegerField(default=0)
    skills = models.ManyToManyField(Skills, related_name='player_skill_instance', blank=True)
    intelligence = models.IntegerField(default=5)
    strength = models.IntegerField(default=5)
    dexterity = models.IntegerField(default=5)
    vitality = models.IntegerField(default=5)
    available_points = models.IntegerField(default=28)

    def increase_exp(self, exp):
        self.exp = exp
        self.save(update_fields=["exp"])

    @property
    def total_won(self) -> int:
        """
        Возвращает количество побед игрока в играх.
        """
        return self.won_games.count()

    @property
    def total_games(self) -> int:
        """
        Возвращает кол-во участий в играх.
        """
        return self.games.count()

    def __str__(self):
        return f'murren_player_{self.murren_player}'


class MurrGameRoom(models.Model):
    room_name = models.CharField(max_length=256)
    room_leader = models.ForeignKey(Murren, on_delete=models.CASCADE)
    is_available = models.BooleanField(default=True, blank=False, null=False)

    def set_available(self, status: bool):
        self.is_available = status
        self.save(update_fields=["is_available"])

    def __str__(self):
        return self.room_name

    @property
    def link(self):
        return f'/ws/murr_game_room/{self.room_name}/'


class FirstGroup(models.Model):
    room_instance = models.ForeignKey(MurrGameRoom, related_name='first_group_in_murr_game_room',
                                      on_delete=models.CASCADE)
    murren_player = models.ForeignKey(Murren, related_name='first_group_murren_player', null=True, blank=True,
                                      on_delete=models.CASCADE)
    group_len = models.IntegerField(null=True, blank=True)


class SecondGroup(models.Model):
    room_instance = models.ForeignKey(MurrGameRoom, related_name='second_group_in_murr_game_room',
                                      on_delete=models.CASCADE)
    murren_player = models.ForeignKey(Murren, related_name='second_group_murren_player', null=True, blank=True,
                                      on_delete=models.CASCADE)
    group_len = models.IntegerField(null=True, blank=True)


class MurrBattleRoomStats(models.Model):
    players = models.ManyToManyField(Player, related_name='games')
    winners = models.ManyToManyField(Player, related_name='won_games')
