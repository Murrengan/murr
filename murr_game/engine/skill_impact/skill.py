import math

from murr_game.engine.skill_impact.base_skill import BaseSkill


def prepare_chosen_skill(now_turn_murren_data, room_data, chosen_skill_name):
    skills_requirements = [now_turn_murren_data, room_data]
    skills = {
        'freezing': Freezing,
        'power_strike': PowerStrike,
        'skip': Skip,
        'strike': Strike,
        'backstab': Backstab
    }
    return skills[chosen_skill_name](*skills_requirements)


class Backstab(BaseSkill):
    def init_damage(self, damage_type, mpv, murren_stats, skill_base_damage):
        damage = self.init_damage_by_type(damage_type, mpv, murren_stats, skill_base_damage)
        for d in damage:
            damage[d] = math.ceil(damage[d] + 0.2 * self.now_turn_murren_data['stats']['dexterity'])
        return damage

    async def armor(self, armor):
        return math.ceil(armor / 100 * 50)

    async def critical_chance(self, damage, *args):
        critical_chance_multiply = 15
        return await super(Backstab, self).critical_chance(damage, critical_chance_multiply)


class Strike(BaseSkill):
    def init_damage(self, damage_type, mpv, murren_stats, skill_base_damage):
        max_stat = max(self.now_turn_murren_data['stats'], key=self.now_turn_murren_data['stats'].get)
        damage = self.init_damage_by_type(damage_type, mpv, murren_stats, skill_base_damage)
        for d in damage:
            damage[d] = math.ceil(damage[d] + 0.3 * self.now_turn_murren_data['stats'][max_stat])
        return damage


class Skip(BaseSkill):
    def init_damage(self, damage_type, mpv, murren_stats, skill_base_damage):
        damage = self.init_damage_by_type(damage_type, mpv, murren_stats, skill_base_damage)
        return damage


class PowerStrike(BaseSkill):
    def init_damage(self, damage_type, mpv, murren_stats, skill_base_damage):
        damage = self.init_damage_by_type(damage_type, mpv, murren_stats, skill_base_damage)
        for d in damage:
            damage[d] = math.ceil(damage[d] + 0.3 * self.now_turn_murren_data['stats']['strength'])
        return damage

    async def evasion(self, evasion):
        return math.ceil(evasion + evasion / 100 * 10)


class Freezing(BaseSkill):
    def init_damage(self, damage_type, mpv, murren_stats, skill_base_damage):
        damage = self.init_damage_by_type(damage_type, mpv, murren_stats, skill_base_damage)
        for d in damage:
            damage[d] = math.ceil(damage[d] + 0.3 * self.now_turn_murren_data['stats']['intelligence'])
        return damage
