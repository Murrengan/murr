async def set_defeated_murren(murr_game_room_data, target_murren_data):
    target_murren_data['hp'] = 0

    if murr_game_room_data.get('defeated_murren_name_by_group') is None:
        murr_game_room_data['defeated_murren_name_by_group'] = {'first_group': [],
                                                                'second_group': []}
    if murr_game_room_data.get('defeated_murren_name_list') is None:
        murr_game_room_data['defeated_murren_name_list'] = []

    murr_game_room_data['defeated_murren_name_by_group'][
        target_murren_data['murren_group']].append(
        target_murren_data['murren_name'])

    murr_game_room_data['defeated_murren_name_list'].append(target_murren_data['murren_name'])

    murr_game_room_data['turn_list'] = [murren for murren in murr_game_room_data['turn_list'] if
                                        murren['murren_name'] not in murr_game_room_data[
                                            'defeated_murren_name_by_group'][
                                            target_murren_data['murren_group']]]
