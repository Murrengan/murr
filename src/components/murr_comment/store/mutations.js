import {
  MUTATIONS_SET_COMMENT,
  MUTATIONS_ADD_COMMENT,
  MUTATIONS_APPEND_COMMENT,
  MUTATIONS_CLEAR_COMMENT,
  MUTATIONS_SET_HAS_NEXT_PAGE,
  MUTATIONS_SET_CURRENT_PAGE,
} from "./type.js";

export default {
  [MUTATIONS_SET_COMMENT](state, payload) {
    state.comments = payload;
  },

  [MUTATIONS_ADD_COMMENT](state, payload) {
    state.comments = [payload, ...state.comments];
  },

  [MUTATIONS_APPEND_COMMENT](state, payload) {
    state.comments = [...state.comments, ...payload];
  },

  [MUTATIONS_CLEAR_COMMENT](state) {
    state.comments = [];
    state.commentsCurrentPage = 1;
    state.commentsHasNextPage = false;
  },

  [MUTATIONS_SET_HAS_NEXT_PAGE]: (state, payload) =>
    (state.commentsHasNextPage = payload),

  [MUTATIONS_SET_CURRENT_PAGE]: (state, payload) =>
    (state.commentsCurrentPage = payload),
};
