import time

from channels.db import database_sync_to_async
from django.conf import settings
from django.contrib.auth import get_user_model

from murr_websocket.consumers.base import BaseMurrWebSocketConsumer
from murr_websocket.models import MurrWebSocketMessage
from murr_websocket.utils import clean_html, wrap_links_in_tags
from murren.serializers import MurrenSerializer

Murren = get_user_model()

LIMIT_MESSAGES_ON_PAGE = 30


class MurrChatConsumer(BaseMurrWebSocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scope['murr_ws_type'] = 'murr_chat'

    async def connect(self):
        await super().connect()
        self.murr_chat_name = self.scope['murr_ws_name']

        await self.channel_layer.group_add(self.murr_chat_name, self.channel_name)
        await self.add_murren_to_murr_websocket_members(self.scope['murren'], self.murr_websocket_instance.id)
        self.murr_ws_member = self.scope['murren'].murr_ws_member \
            .filter(murr_ws_instance=self.murr_websocket_instance) \
            .first()
        self.murr_ws_member.set_online(True)

        murr_websocket_data = await self.get_websocket_data(self.murr_websocket_instance.id)
        murr_websocket_data['murrens_in_ws_data'] = await self.get_online_murrens(murr_websocket_data)
        await self._group_send(murr_websocket_data, gan='join_to_murr_chat')

    async def get_online_murrens(self, ws_data):
        return list(filter(lambda murren: murren['is_online'], ws_data['murrens_in_ws_data']))

    async def gan__list_messages(self, event):
        offset = event['data'].get('offset', 0)
        messages = await self.get_messages(offset)
        return await self._send_message(messages, gan=event['gan'])

    async def validate_message(self, message):
        if isinstance(message, str):
            return clean_html(message, allow_tags=['p', 'br']).strip()

    async def gan__send_chat_message(self, gan):
        data = {
            'murren_name': self.scope['murren'].username,
            'murren_avatar': MurrenSerializer.get_avatar(None, self.scope['murren']),
            'message': 'message_stub',
        }
        message = gan['data'].get('message', '')

        await self.murr_chat_anti_spam_system(gan, data, message)

        message = await self.validate_message(message)
        if not message:
            return await self._trow_error({'detail': 'Invalid Message'})

        message = wrap_links_in_tags(message)
        await self.save_message(message, self.scope['murren'])
        data = {
            'murren_name': self.scope['murren'].username,
            'murren_avatar': MurrenSerializer.get_avatar(None, self.scope['murren']),
            'message': message,
        }
        return await self._group_send(data, gan=gan['gan'])

    @database_sync_to_async
    def get_messages(self, offset):
        messages = MurrWebSocketMessage.objects \
            .select_related('murr_ws_member') \
            .filter(murr_ws_instance=self.murr_websocket_instance) \
            .order_by('-id')
        data = {
            'messages': [],
            'offset': offset,
            'last_message': messages.count() - (LIMIT_MESSAGES_ON_PAGE + offset) < 0
        }
        for message in messages[offset:offset + LIMIT_MESSAGES_ON_PAGE:-1]:
            data['messages'].append({
                'id': message.id,
                'murren_name': message.murr_ws_member.username,
                'murren_avatar': MurrenSerializer.get_avatar(None, message.murr_ws_member),
                'message': message.murr_ws_message
            })
        return data

    async def disconnect(self, code):
        self.murr_ws_member.set_online(False)
        await self._group_send(self.scope['murren'].username,
                               gan='disconnect')
        await self.channel_layer.group_discard(self.murr_chat_name, self.channel_name)
        await super().disconnect(code=code)

    async def murr_chat_anti_spam_system(self, gan, data, message):
        murren = await self.get_murren(self.scope['murren'].id)
        if murren.is_banned is True:
            data['message'] = 'Ты забанен за спам - пиши в поддержку за разбаном'
            await self._send_message(data, gan=gan['gan'])
            raise Exception("Murr_chat anti_spam system - Murren is banned")

        if len(message) > settings.MURR_CHAT_MESSAGE_CHARACTERS_LEN_FOR_BAN:
            self.ban_murren(self.scope['murren'].id)
            data['message'] = 'Думаешь самый умный? Держи бан за спам! Пиши в поддержку за разбаном'
            await self._send_message(data, gan=gan['gan'])
            raise Exception("Murr_chat anti_spam system - too large message")

        if not self.scope.get('anti_spam'):
            self.scope['anti_spam'] = {
                'first_message_time': time.time(),
                'messages_list': []
            }

        self.scope['anti_spam']['messages_list'].append(message)

        if (time.time() - self.scope['anti_spam'][
            'first_message_time'] < settings.MURR_CHAT_ANTI_SPAM_TIMEOUT_IN_SEC) and \
                (len(self.scope['anti_spam']['messages_list']) > settings.MURR_CHAT_MESSAGES_COUNT_BEFORE_BAN):
            await self.ban_murren(self.scope['murren'].id)
            data['message'] = 'Ты забанен за спам - пиши в поддержку за разбаном'
            await self._send_message(data, gan=gan['gan'])
            raise Exception("Murr_chat anti_spam system - too many message")

        if time.time() - self.scope['anti_spam']['first_message_time'] > settings.MURR_CHAT_ANTI_SPAM_TIMEOUT_IN_SEC:
            self.scope['anti_spam'] = {
                'first_message_time': time.time(),
                'messages_list': []
            }
