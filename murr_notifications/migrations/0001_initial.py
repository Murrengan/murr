# Generated by Django 3.0.8 on 2020-12-01 14:48

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_enum_choices.choice_builders
import django_enum_choices.fields
import murr_notifications.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('murr_card', '0008_auto_20201018_1541'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseNotification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sent_time', models.DateTimeField(auto_now=True, verbose_name='Время отправки')),
                ('notification_type', django_enum_choices.fields.EnumChoiceField(choice_builder=django_enum_choices.choice_builders.value_value, choices=[('feed', 'feed'), ('subscribe', 'subscribe')], enum_class=murr_notifications.models.NotificationType, max_length=9, verbose_name='Тип уведомления')),
                ('to_murren', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='received_notifications', to=settings.AUTH_USER_MODEL, verbose_name='получатель')),
            ],
            options={
                'verbose_name': 'уведомление',
                'verbose_name_plural': 'уведомления',
            },
        ),
        migrations.CreateModel(
            name='SubscribeNotification',
            fields=[
                ('basenotification_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='murr_notifications.BaseNotification')),
                ('from_murren', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Отправитель')),
            ],
            options={
                'verbose_name': 'уведомление',
                'verbose_name_plural': 'уведомления связанные с подпиской',
            },
            bases=('murr_notifications.basenotification',),
        ),
        migrations.CreateModel(
            name='FeedNotification',
            fields=[
                ('basenotification_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='murr_notifications.BaseNotification')),
                ('murr_card', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='murr_card.MurrCard', verbose_name='Мурр')),
            ],
            options={
                'verbose_name': 'уведомление',
                'verbose_name_plural': 'уведомления связанные c новостной лентой',
            },
            bases=('murr_notifications.basenotification',),
        ),
    ]
