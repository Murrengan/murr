#!/bin/sh

#if [ "$DJANGO_DB_ENGINE" = "django.db.backends.postgresql" ]
## База данных стартует не сразу - мы в цикле ждем ответа через дефолтную утилиту линукс nc:
## netcat https://losst.ru/komanda-nc-v-linuxnc
#
#then
#    echo "Waiting for postgres..."
#
#    while ! nc -z $DJANGO_DB_HOST $DJANGO_DB_PORT; do
#      # Если у нас нет ответа по запросу db:5432 спим 0.1 секунду
#      sleep 0.1
#    done
#
#    echo "PostgreSQL started"
#fi

python manage.py migrate
python manage.py collectstatic --no-input --clear
python manage.py prepare_stand_dev
python manage.py runserver 0.0.0.0:8000

exec "$@"
# File | File Properties | Line Separators | LF - для запуска на windows поменять изменить line sepavrators на LF